import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
    name: 'activeFilter',
    pure: false
})
export class ActiveFilterPipe implements PipeTransform {
    transform(items: any[], filter:any): any {
        return items.filter(item => item.active);
    }
}
