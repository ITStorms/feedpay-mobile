import { PersonalLoaylty } from './../../providers/loyalty/personalLoyalty';
import { Loyalty } from './../../providers/loyalty/loyalty.model';
import { LoyaltyProvider } from './../../providers/loyalty/loyalty';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LoyaltyConfigPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-loyalty-config',
  templateUrl: 'loyalty-config.html',
})
export class LoyaltyConfigPage {

  p: PersonalLoaylty = new PersonalLoaylty();
  constructor(public navCtrl: NavController, public navParams: NavParams, private loyaltyProvider: LoyaltyProvider) {
    this.p.idActive = 1;
    this.p.loyalties = [];
    this.p.loyalties = this.loyaltyProvider.getLoayltyPrograms();
  }

  ionViewDidLoad() {
  }

  toggleProgram(index: number){
    console.log("passei");
    this.loyaltyProvider.activateDeactivateProgram(index);
    this.loyaltyProvider.setLoyaltyPrograms(this.p.loyalties);
  }

}
