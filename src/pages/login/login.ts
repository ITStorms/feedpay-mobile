import { OrderPage } from './../order/order';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {

  }

  goHome(){

  let loading = this.loadingCtrl.create({
    content: 'Por favor aguarde...'
  });

  let nCtrl = this.navCtrl;

  loading.present();

  setTimeout(() => {
    loading.dismiss();
    nCtrl.setRoot(OrderPage);
  }, 3000);

  }

}
