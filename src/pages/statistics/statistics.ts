import { RankingPage } from './../ranking/ranking';
import { StatisticDetailedPage } from './../statistic-detailed/statistic-detailed';
import { StatisticGeneralPage } from './../statistic-general/statistic-general';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the StatisticsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {

  general = StatisticGeneralPage;
  detailed = StatisticDetailedPage;
  ranking = RankingPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

}
