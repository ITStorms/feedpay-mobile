import { OrderPage } from './../order/order';
import { PipeTransform } from '@angular/core';
import { Survey } from './../../providers/survey/survey.model';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, AlertController } from 'ionic-angular';
import { SurveyProvider } from "../../providers/survey/survey";
import { CurrencyPipe } from "@angular/common";

/**
 * Generated class for the SurveyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
  providers: [CurrencyPipe]
})
export class SurveyPage {
  @ViewChild(Slides) slides: Slides;

  pesquisa: Survey;
  questionRate: number[] = [0, 0, 0, 0, 0];
  rates: number[] = [1, 2, 3, 4, 5];
  orderValue: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private surveyProvider: SurveyProvider, public alertCtrl: AlertController) {
    this.pesquisa = surveyProvider.getSurvey();
  }

  ionViewDidLoad() {
    this.slides.lockSwipes(true);
    this.orderValue = this.navParams.get('value');
  }

  setRate(r: number, index: number) {
    this.questionRate[index] = r;

    this.nextQuestion();
  }


  nextQuestion() {
    if (this.slides.getActiveIndex() === (this.slides.length() - 2)) {
      console.log("só salvar!");
    }
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);

      if (this.slides.isEnd()) {
        console.log("entrei");
        var val = (new CurrencyPipe('pt-BR')).transform(this.orderValue, 'BRL', true);
        let alert = this.alertCtrl.create({
          title: `Obrigado por participar!`,
          message: `Valor: ${val}. Vire a máquina para pagar`,
          buttons: [{
            text: 'OK',
            handler: () => {
              this.navCtrl.setRoot(OrderPage)
            }
          }]
        });
        alert.present();
      }
  }

}
