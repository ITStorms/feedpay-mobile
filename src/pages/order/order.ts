import { SurveyPage } from './../survey/survey';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the OrderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  value: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
  }

  goToSurvey(){
    this.navCtrl.push(SurveyPage, { value: this.value });
  }

  pay(){
    let loader = this.loadingCtrl.create({
      content: "Por favor aguarde...",
      duration: 3000
    });
    let alert = this.alertCtrl.create({
      title: 'Vire a máquina para pagar!',
      buttons: ['OK']
    });
    alert.present();
  }
}
