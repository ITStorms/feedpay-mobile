import { Survey } from './../../providers/survey/survey.model';
import { SurveyProvider } from './../../providers/survey/survey';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SurveyConfigPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-survey-config',
  templateUrl: 'survey-config.html',
})
export class SurveyConfigPage {

  survey: Survey;
  constructor(public navCtrl: NavController, public navParams: NavParams, private surveyProvider: SurveyProvider) {
    this.survey = surveyProvider.getSurvey();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyConfigPage');
  }

  toggleGenre(){
    this.surveyProvider.activateDeactivateGenre();
  }

  toggleAge(){
    this.surveyProvider.activateDeactivateAge();
  }

  toggleQuestion(index: number) {
    this.surveyProvider.activateDeactivateQuestion(index);
  }
}
