import { StatisticsPage } from './../pages/statistics/statistics';
import { LoyaltyConfigPage } from './../pages/loyalty-config/loyalty-config';
import { SurveyConfigPage } from './../pages/survey-config/survey-config';
import { SurveyPage } from './../pages/survey/survey';
import { OrderPage } from './../pages/order/order';
import { LoginPage } from './../pages/login/login';
import { ConfigProvider } from './../providers/config/config';
import { IntroPage } from './../pages/intro/intro';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html',
  providers: [ ConfigProvider ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, path: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private config: ConfigProvider, private alertCtrl: AlertController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Venda', component: OrderPage, path: "assets/icon/coin.png" },
      { title: 'Fidelidade', component: LoyaltyConfigPage, path: "assets/icon/loyal.png" },
      { title: 'Pesquisa', component: SurveyConfigPage, path: "assets/icon/survey.png" },
      { title: 'Estatísticas', component: StatisticsPage, path: "assets/icon/line-chart.png" },
      { title: 'Sobre', component: IntroPage, path: "assets/icon/info.png" }
    ];

    let cfg = config.getConfigData();
    console.log(cfg);
    if(cfg == null){
     this.rootPage = IntroPage;
        config.setConfigData(false);
      } else {
        this.rootPage = LoginPage;
      }

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    let n = this.nav;

    let alert = this.alertCtrl.create({
    title: 'Sair',
    message: 'Deseja mesmo sair?',
    buttons: [
      {
        text: 'Não',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Sim',
        handler: () => {
          n.setRoot(LoginPage);
        }
      }
    ]
  });
  alert.present();
  }

}
