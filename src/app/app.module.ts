import { RankingPage } from './../pages/ranking/ranking';
import { StatisticDetailedPage } from './../pages/statistic-detailed/statistic-detailed';
import { StatisticGeneralPage } from './../pages/statistic-general/statistic-general';
import { StatisticsPage } from './../pages/statistics/statistics';
import { LoyaltyConfigPage } from './../pages/loyalty-config/loyalty-config';
import { ActiveFilterPipe } from './../util/activeFilter';
import { SurveyConfigPage } from './../pages/survey-config/survey-config';
import { SurveyPage } from './../pages/survey/survey';
import { SignupPage } from './../pages/signup/signup';
import { OrderPage } from './../pages/order/order';
import { LoginPage } from './../pages/login/login';
import { IntroPage } from './../pages/intro/intro';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConfigProvider } from '../providers/config/config';
import { SurveyProvider } from '../providers/survey/survey';
import { CurrencyMaskModule } from "ng2-currency-mask";

import { HttpModule } from '@angular/http';
import { LoyaltyProvider } from '../providers/loyalty/loyalty';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    IntroPage,
    LoginPage,
    OrderPage,
    SignupPage,
    SurveyPage,
    SurveyConfigPage,
    ActiveFilterPipe,
    LoyaltyConfigPage,
    StatisticsPage,
    StatisticGeneralPage,
    StatisticDetailedPage,
    RankingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    CurrencyMaskModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    IntroPage,
    LoginPage,
    OrderPage,
    SignupPage,
    SurveyPage,
    SurveyConfigPage,
    LoyaltyConfigPage,
    StatisticsPage,
    StatisticGeneralPage,
    StatisticDetailedPage,
    RankingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConfigProvider,
    SurveyProvider,
    LoyaltyProvider
  ]
})
export class AppModule {}
