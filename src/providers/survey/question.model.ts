export interface Question {
    question: string,
    active: boolean,
    icon: string,
    color:string
}