import { Question } from './question.model';

export interface Survey {
    sexo: any,
    idade: any,
    questions: Question[];
}