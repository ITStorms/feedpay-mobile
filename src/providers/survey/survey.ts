import { Survey } from './survey.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SurveyProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SurveyProvider {
  
  survey: Survey = {
    sexo: { value: "", active: false},
    idade: { value: 1, active: false},
    questions: [
      { question: "Você gostou do atendimento?", active: false, icon: "md-thumbs-up", color: "light-icon" },
      { question: "O ambiente estava agradável?", active: false, icon: "ios-home", color: "light-icon"  },
      { question: "A comida chegou no tempo ideal?", active: false, icon: "time", color: "light-icon" },
      { question: "Como você avaliaria a comida?", active: false, icon: "restaurant", color: "light-icon" },
      { question: "Você nos indicaria a um amigo?", active: false, icon: "md-contacts", color: "light-icon" }
    ]
  }

  constructor(public http: Http) {
  }

  getSurvey(){
    return this.survey;
  }

  activateDeactivateQuestion(index: number) {
    this.survey.questions[index].active = !this.survey.questions[index].active;
  }

  activateDeactivateGenre(){
    this.survey.sexo.active = !this.survey.sexo.active;
  }

  activateDeactivateAge(){
    this.survey.idade.active = !this.survey.idade.active;
  }
}
