import { Loyalty } from './loyalty.model';
import { Survey } from './../survey/survey.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the LoyaltyProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoyaltyProvider {
  programs: Loyalty[] = [{
    id: 1,
    name: "Compre 2 Leve 1",
    description: "",
    fixedDiscount: true,
    value: 1.0,
    qtd: 2,
    active: false
  }, {
    id: 2,
    name: "Compre 5 e ganhe 10% de desconto",
    description: "",
    fixedDiscount: false,
    value: 10,
    qtd: 5,
    active: false
  }
    , {
    id: 3,
    name: "Compre 2 e ganhe 1% de desconto",
    description: "",
    fixedDiscount: false,
    value: 1,
    qtd: 2,
    active: false
  }]

  constructor(public http: Http) {
  }

  getLoayltyPrograms(): Loyalty[] {
    return this.programs;
  }

  setLoyaltyPrograms(array: Loyalty[]) {
    this.programs = array;
  }

  activateDeactivateProgram(index: number) {
    this.programs[index].active = !this.programs[index].active;

    if (this.programs[index].active) {
      this.programs.forEach(element => {
        if (this.programs[index].id !== element.id)
          element.active = false;
      });
    }
  }

  createLoyaltyProgram(loyalty: Loyalty) {
    this.programs.push(loyalty);
  }

  deleteLoyaltyProgram(id: number) {
    this.programs.forEach((element, index, obj) => {
      if (element.id === id) {
        obj.splice(index, 1);
      }
    });
  }
}
