export interface Loyalty {
    id: number,
    name: string,
    description?: string,
    fixedDiscount: boolean,
    qtd: number,
    value: number,
    active: boolean
}